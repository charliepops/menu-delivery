import {
  ADDING_PORTION,
  ADDED_PORTION,
  UPDATE_PORTIONS,
} from 'actions/portions';

const initialState = {
  data: [],
  isFetching: false
};

function portions(state = initialState, action) {

  switch (action.type) {

    case ADDING_PORTION:
      return Object.assign({}, state, { isFetching: true });

    case ADDED_PORTION:
      return Object.assign({}, state, { isFetching: false });

    case UPDATE_PORTIONS:
      return Object.assign({}, state, { data: action.portions });

    default:
      return state;
  }

}

export default portions;
