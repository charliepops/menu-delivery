import { combineReducers } from 'redux';
import portions from './portions';
import profile from './profile';

const rootReducer = combineReducers({
  portions,
  profile
});

export default rootReducer;
