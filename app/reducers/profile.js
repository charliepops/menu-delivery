import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  SET_USER_DATA,
  LOGOUT
} from 'actions/profile';

const initialState = {
  data: null,
  isFetching: false
};

function profile(state = initialState, action) {

  switch (action.type) {

    case LOGIN_REQUEST:
      return Object.assign({}, state, { isFetching: true});

    case LOGIN_SUCCESS:
      return Object.assign({}, state, { isFetching: false});

    case SET_USER_DATA:
      return Object.assign({}, state, { data: action.data});

    case LOGOUT:
      return Object.assign({}, initialState);

    default:
      return state;
  }

}

export default profile;
