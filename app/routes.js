import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from 'components/App';
import Home from 'components/pages/Home';
import Plans from 'components/pages/Plans';
import Menu from 'components/pages/Menu';
import About from 'components/pages/About';
import Contact from 'components/pages/Contact';
import Admin from 'components/admin/Admin';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="plans" component={Plans}/>
    <Route path="menu" component={Menu}/>
    <Route path="about" component={About}/>
    <Route path="contact" component={Contact}/>
    <Route path="admin" component={Admin}/>
  </Route>
);
