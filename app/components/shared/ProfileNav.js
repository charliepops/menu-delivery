import React, { Component } from 'react';
import { loginUserFacebook, logout } from 'actions/profile';
import { connect } from 'react-redux';
import classNames from 'classnames';

class ProfileNav extends Component {

  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
    this.onLogin = this.onLogin.bind(this);
    this.onLogout = this.onLogout.bind(this);
  }

  onLogin() {
    this.props.dispatch(loginUserFacebook());
  }

  onLogout() {
    this.props.dispatch(logout());
  }

  onChangeMenu() {
    this.setState({
      active: !this.state.active
    })
  }

  render() {
    const { profile } = this.props;

    const dropdownMenu = classNames({
      'c-menu-dropdown': true,
      'c-menu-dropdown--active': this.state.active,
      'c-menu-dropdown--secondary-header ': true,
      'u-unstyled-list': true
    })

    return (
      <section className="header-secondary">
        <div className="content-wrapper">
          <div className="c-profile-nav u-pull-right">
            {
              profile.data ?

              <div className="c-profile-nav__text">
                <ul className="c-profile-nav__list u-unstyled-list">
                  <li className="c-profile-nav__list-item">
                    Cart
                  </li>
                  <li className="c-profile-nav__list-item">
                    <a href="#" className="c-profile-nav__link c-profile-nav__link--menu" onClick={this.onChangeMenu.bind(this)}>
                      <img className="c-profile-nav__media" src={profile.data.image} alt="{profile.data.name}" />
                      {profile.data.name}
                    </a>
                    <ul className={dropdownMenu}>
                      <li className="c-menu-dropdown__item">
                        <a href="" className="c-menu-dropdown__link">
                          <i className="c-menu-dropdown__icon c-icon c-icon-user"></i>
                          <span className="c-menu-dropdown__text">Profile</span>
                        </a>
                      </li>
                      <li className="c-menu-dropdown__item">
                        <a href="" className="c-menu-dropdown__link">
                          <i className="c-menu-dropdown__icon c-icon c-icon-alert"></i>
                          <span className="c-menu-dropdown__text">Alerts</span>
                        </a>
                      </li>
                      <li className="c-menu-dropdown__item">
                        <a href="" className="c-menu-dropdown__link">
                          <i className="c-menu-dropdown__icon c-icon c-icon-message"></i>
                          <span className="c-menu-dropdown__text">Messages</span>
                        </a>
                      </li>
                      <li className="c-menu-dropdown__item">
                        <a href="#" className="c-menu-dropdown__link" onClick={this.onLogout}>
                          <i className="c-menu-dropdown__icon c-icon c-icon-logout"></i>
                          <span className="c-menu-dropdown__text">Sign out</span>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>

              :

              <div className="c-profile-nav__text">
                <ul className="c-profile-nav__list u-unstyled-list">
                  <li className="c-profile-nav__list-item">
                    <a href="#" className="c-profile-nav__link" onClick={this.onLogin}>Sign in</a>
                    {/* <button className="btn btn--small" onClick={this.onLogin}>Sign in</button> */}
                  </li>
                  <li className="c-profile-nav__list-item">
                    <a href="#" className="btn btn--secondary btn--small">Sign up</a>
                  </li>
                </ul>
              </div>
            }
          </div>
        </div>
      </section>
    );
  }

}

ProfileNav.propTypes = {
  dispatch: React.PropTypes.func,
  profile: React.PropTypes.object
};

function mapStateToProps(state) {
  return {
    profile: state.profile
  }
}

export default connect(mapStateToProps)(ProfileNav);
