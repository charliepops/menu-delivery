import React, { Component } from 'react';

class Portions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
    };
  }

  render() {
    const { name } = this.state;
    const { portions, onAdd } = this.props;
    return (
      <div>
        <p>
          <input
            ref="portion"
            type="text"
            placeholder="porcion"
            onChange={ e => this.setState({name: e.target.value}) }
            value={name}
          />
          <button onClick={() => onAdd(this.refs.portion.value)}>agregar</button>
        </p>
        <div>
          <ul>
          {Object.keys(portions.data).map( key => {
            const portion = portions.data[key];
            return <li key={key}>{portion.name}</li>;
          })}
          </ul>
        </div>
      </div>
    );
  }

}

Portions.propTypes = {
  portions: React.PropTypes.object,
  onAdd: React.PropTypes.func
};

export default Portions;
