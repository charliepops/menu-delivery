import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addPortion, updatePortions } from 'actions/portions';
import { listenToList } from 'utils/firebase';
import Portions from './Portions';

class Admin extends Component {

  constructor(props) {
    super(props);
    this.onPortionAdd = this.onPortionAdd.bind(this);
  }

  componentDidMount() {
    listenToList('portions', snapshot => {
      this.props.dispatch(updatePortions(snapshot.val()));
    });
  }

  onPortionAdd(portionName) {
    this.props.dispatch(addPortion(portionName));
  }

  render() {
    return (
      <div>
        <Portions
          portions={this.props.portions}
          onAdd={this.onPortionAdd}
        />
      </div>
    );
  }

}

Admin.propTypes = {
  dispatch: React.PropTypes.func,
  portions: React.PropTypes.object
};

function mapStateToProps(state) {
  return {
    portions: state.portions
  }
}

export default connect(mapStateToProps)(Admin);
