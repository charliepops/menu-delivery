import React, { Component } from 'react';
import ProfileNav from '../shared/ProfileNav';
import Header from '../shared/Header';

class About extends Component {

  render() {

    return (
      <div>
        <ProfileNav/>
        <Header/>
        <main role="main">
          <h1>About</h1>
        </main>
      </div>
    );
  }

}

export default About;
