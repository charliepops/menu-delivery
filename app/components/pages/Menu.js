import React, { Component } from 'react';
import ProfileNav from '../shared/ProfileNav';
import Header from '../shared/Header';

class Menu extends Component {

  render() {

    return (
      <div>
        <ProfileNav/>
        <Header/>
        <main role="main">
          <h1>Menu</h1>
        </main>
      </div>
    );
  }

}

export default Menu;
