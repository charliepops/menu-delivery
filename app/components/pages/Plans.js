import React, { Component } from 'react';
import ProfileNav from '../shared/ProfileNav';
import Header from '../shared/Header';

class Plans extends Component {

  render() {

    return (
      <div>
        <ProfileNav/>
        <Header/>
        <main role="main">
          <h1>Plans</h1>
        </main>
      </div>
    );
  }

}

export default Plans;
