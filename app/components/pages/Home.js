import React, { Component } from 'react';
import ProfileNav from '../shared/ProfileNav';
import Header from '../shared/Header';

class Home extends Component {

  render() {

    return (
      <div>
        {/* <section className="c-notifications">
          <div className="c-notifications__single">
            <div className="content-wrapper">
              <span className="c-notifications__text">
                New update coming up this week! Wait for it...
              </span>
              <a href="" className="btn btn--small btn--secondary u-pull-right">Check it!</a></div>
          </div>
        </section> */}
        <ProfileNav/>
        <Header/>
        <main role="main">
          <section className="c-hero c-hero--primary">
            <img className="c-hero__bg-img" src="app/assets/images/food-2.jpg" alt="" />
            <div className="c-hero__wrap">
              <div className="content-wrapper">
                <div className="row">
                  <div className="col-sm-6">
                    <div className="c-hero__text">
                      <h2 className="c-title-primary c-color-white">DELICIOUS FOOD <br/> IN YOUR
                        <span className="c-rotator-word">
                          <span>HOME</span>
                          <span>OFFICE</span>
                          <span>ANYWHERE</span>
                        </span>
                      </h2>
                      <p className="c-title-quaternary c-color-white">You probably don't have time, so let us take care of your meal prep.</p>
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <iframe src="https://player.vimeo.com/video/27243869" frameBorder="0" width="100%" height="280"></iframe>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section className="c-block-feature">

          </section>
        </main>

      </div>
    );
  }

}

export default Home;
