import React, { Component } from 'react';

class App extends Component {

  render() {
    // only render route component
    return <div>{this.props.children}</div>;
  }

}

App.propTypes = {
  children: React.PropTypes.element
};

export default App;
