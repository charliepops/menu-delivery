import { writePortionData } from 'utils/firebase';

export const ADDING_PORTION = 'ADDING_PORTION';
export const ADDED_PORTION = 'ADDED_PORTION';
export const UPDATE_PORTIONS = 'UPDATE_PORTIONS';

export function addPortion(name) {
  return dispatch => {
    dispatch({
      type: ADDING_PORTION
    });
    writePortionData({name}).then( () => {
      dispatch({
        type: ADDED_PORTION
      });
    });
  }
}

export function updatePortions(portions) {
  return {
    type: UPDATE_PORTIONS,
    portions
  }
}
