import { loginFacebookFirebase, logoutFirebase } from 'utils/firebase';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT = 'LOGOUT';
export const SET_USER_DATA = 'SET_USER_DATA';

export function loginUserFacebook() {
  return dispatch => {

    dispatch({
      type: LOGIN_REQUEST
    });

    loginFacebookFirebase().then( () => {
      dispatch({
        type: LOGIN_SUCCESS,
      });
    });

  }
}

export function logout() {
  logoutFirebase();
  return {
    type: LOGOUT
  }
}

export function setUserData(data) {
  return {
    type: SET_USER_DATA,
    data
  }
}
