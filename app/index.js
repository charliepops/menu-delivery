import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from 'store/configureStore';
import { initFirebase } from 'utils/firebase';
import { Router, browserHistory } from 'react-router';
import routes from 'routes';

const store = configureStore();

initFirebase(store).then(() => {

  render(
    <Provider store={store}>
      <Router history={browserHistory}>
        {routes}
      </Router>
    </Provider>,
    document.getElementById('app')
  );

});
