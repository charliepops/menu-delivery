import firebase from 'firebase';
import config from 'config';
import { setUserData } from 'actions/profile';

export function initFirebase(store) {
  return new Promise((resolve) => {
    firebase.initializeApp(config.firebase);
    firebase.auth().onAuthStateChanged( user => {
      if (user) {
        const data = getUserDataFacebook(user);
        store.dispatch(setUserData(data));
        writeUserData(data)
      }
      resolve(user);
    });
  })
}

export function loginFacebookFirebase() {
  return new Promise((resolve) => {
    var provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithPopup(provider).then(function(result) {
      resolve(getUserDataFacebook(result.user));
    });
  });
}

function getUserDataFacebook({displayName, email, photoURL, uid}) {
  return {
    name: displayName,
    email: email,
    image: photoURL,
    uid: uid
  }
}

export function logoutFirebase() {
  firebase.auth().signOut();
}

function writeUserData(data) {
  firebase.database().ref('users/' + data.uid).set(data);
}

export function writePortionData(data) {
  return new Promise( (resolve) => {
    var key = firebase.database().ref().child('portions').push().key;
    data.uid = key;
    firebase.database().ref().update({ ['portions/' + key]: data });
    resolve(data);
  });
}

export function listenToList(child, handler, once = false) {
  firebase.database().ref(child)[once ? 'once' : 'on']('value', handler);
}
