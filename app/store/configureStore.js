import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import rootReducer from 'reducers';

export default function configureStore() {

  const store = createStore(
    rootReducer,
    applyMiddleware(thunkMiddleware, createLogger({collapsed: true}))
  );

  return store
}
