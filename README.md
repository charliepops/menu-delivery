# Menu Delivery

Una aplicacion para comelones.

## Usage

- Clone this project `git clone https://charliepops@bitbucket.org/charliepops/menu-delivery.git`.
- Go to the project's directory.
- Run `npm install` to install dependencies.
- Run `npm start` to run development server.
- Run `npm run build` to create production files in **build** directory.
